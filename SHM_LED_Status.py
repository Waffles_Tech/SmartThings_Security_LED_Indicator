import RPi.GPIO as GPIO
import time
import requests

LEDPINR = 17
LEDPING = 23

def setup():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(LEDPINR,GPIO.OUT,initial=GPIO.LOW)
    GPIO.setup(LEDPING,GPIO.OUT,initial=GPIO.LOW)

def main():
    while True:
        headers = {'Authorization': 'Bearer API-TOKEN',}
        response = requests.get('URL/API/ENDPOINT', headers=headers)
        SHM_Status = response.text.replace('[','').replace(']','')
        if SHM_Status == 'false':
            GPIO.output(LEDPINR,GPIO.HIGH)
            GPIO.output(LEDPING,GPIO.LOW)
        elif SHM_Status == 'true':
            GPIO.output(LEDPINR,GPIO.LOW)
            GPIO.output(LEDPING,GPIO.HIGH)
        else:
            GPIO.output(LEDPINR,GPIO.HIGH)
            GPIO.output(LEDPING,GPIO.HIGH)
        time.sleep(30)

def destroy():
    GPIO.output(LEDPINR,GPIO.LOW)
    GPIO.output(LEDPING,GPIO.LOW)
    GPIO.cleanup()

if __name__ == '__main__':
    setup()
    try:
        main()
    except KeyboardInterrupt:
        destroy()
    except SystemExit:
        destroy()